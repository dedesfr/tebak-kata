easy = ["armadillo", "elephant", "lizard", "crocodile", "turtle", "antelope"]
hard = ["mountain lion", "red panda", "sugar glider"]
impossible = ["uvuvwevwevwe onyetenyevwe ugwemubwem osas", "tekokak labu parang"]

def shuffle_string(string)
  splitArr = string.split(" ")
  splitResult = []

  splitArr.each do |w|
    splitResult << w.split('').shuffle.join
  end
  
  return splitResult.join(' ')
end

def playGame(level)
  point = 0
  counter = 0
  level.shuffle.each do |e|
    randomString = shuffle_string(e)
    
    loop do
      puts "SALAH! silahkan coba lagi" if counter > 0
      print "\n"
      puts "Tebak Kata: #{randomString}"
      print "Jawab: "
      guess = gets
      print "\n"
      counter += 1
      break if guess.chomp.eql? e
    end 

    point += 1
    counter = 0
    puts "BENAR point anda : #{point}"
    print "\n"
  end
  puts "Selamat anda menyelesaikan level ini dengan point #{point}"
end


puts "Level : "
puts " - easy"
puts " - hard"
puts " - impossible"
print "Choose Level : "
level = gets.chomp

case level
  when 'easy'
    playGame(easy)
  when 'hard'
    playGame(hard)
  when 'impossible'
    playGame(impossible)
  else
    puts "level tidak ditemukan"
end